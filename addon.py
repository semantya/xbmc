#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from xbmcswift import Plugin, download_page
from xbmcswift.ext.playlist import playlist
from resources.lib.getflashvideo import get_flashvideo_url, YouTube
from BeautifulSoup import BeautifulSoup as BS, SoupStrainer as SS
from urllib import urlencode
from urlparse import urljoin
from urllib2 import urlopen
import re
try:
    import json
except ImportError:
    import simplejson as json
from xbmcswift import xbmc, xbmcgui

__plugin__ = 'ICE Saloon'
__plugin_id__ = 'plugin.video.saloon'

plugin = Plugin(__plugin__, __plugin_id__, __file__)

plugin.register_module(playlist, url_prefix='/_playlist')

BASE_URL = 'http://watch.maher.rest/watch'

def do_request(url, payload=None):
    req = urlopen(BASE_URL + url)
    
    resp = req.read()
    
    resp = json.loads(resp)
    
    return resp

def get_entry(item):
    resp = {
        'url': 'plugin://' + __plugin_id__ + item['url'],
    }
    
    for key,default in dict(label=None, thumbnail=None, is_folder=True, is_playable=False).iteritems():
        if key in item:
            resp[key] = item[key]
        elif default!=None:
            resp[key] = default
    
    for key,subst in dict(plot='summary').iteritems():
        if key in item:
            if 'info' not in resp : resp['info'] = {}
            
            resp['info'][key] = item[subst]
    
    #resp.update({
    #    'context_menu': [(
    #        plugin.get_string(30300),
    #        'XBMC.RunPlugin(%s)' % plugin.url_for(
    #            'playlist.add_to_playlist',
    #            url=plugin.url_for('watch_video', videoid=item['video_id']),
    #            label=item['title']
    #        )
    #    )],
    #})
    
    return resp

######################################################################################

@plugin.route('/', default=True)
def show_homepage():
    items = []
    
    for obj in do_request('/home'):
        item = get_entry(obj)
        
        items.append(item)
    
    return plugin.add_items(items)

@plugin.route('/explore/<kind>/<uuid>/')
def show_explore(kind, uuid):
    items = []
    
    for obj in do_request('/explore/%s/%s/' % (kind, uuid)):
        item = get_entry(obj)
        
        items.append(item)
    
    return plugin.add_items(items)

@plugin.route('/live/<channel_id>/')
def live_watch(channel_id):
    channel = do_request('/live/%s' % channel_id)
    
    li = xbmcgui.ListItem(channel['label'])
    
    xbmc.Player(xbmc.PLAYER_CORE_DVDPLAYER).play(channel['stream'], li)
    
    return []

@plugin.route('/watch/<provider>/<videoid>/')
def watch_video(provider, videoid):
    url = None
    
    if provider.lower()=='youtube':
        url = YouTube.get_flashvideo_url(videoid=videoid)
    
    elif provider.lower()=='saloon':
        url = YouTube.get_flashvideo_url(videoid=videoid)
    
    if url:
        return plugin.set_resolved_url(url)

if __name__ == '__main__': 
    plugin.run()

